(ns plf02.core)

(defn función-associative?-1
  [a]
  (associative? a))

(defn función-associative?-2
  [a]
  (associative? a))

(defn función-associative?-3
  [a]
  (associative? a))

(función-associative?-1 [1 2 3 4])
(función-associative?-2 '(1 2 3 ))
(función-associative?-2 #{:a :b :c})

(defn función-boolean?-1
  [a]
  (boolean? a))

(defn función-boolean?-2
  [a]
  (boolean? a))

(defn función-boolean?-3
  [a]
  (boolean? a))

(función-boolean?-1 [false true])
(función-boolean?-2  true)
(función-boolean?-3 nil)

(defn función-char?-1
  [a]
  (char? a))

(defn función-char?-2
  [a]
  (char? a))

(defn función-char?-3
  [a]
  (char? a))

(función-char?-1 [range 10])
(función-char?-2 (first [\a \b]))
(función-char?-3 "hola")

(defn función-coll?-1
  [a]
  (coll? a))

(defn función-coll?-2
  [a]
  (coll? a))

(defn función-coll?-3
  [a]
  (coll? a))

(función-coll?-1 [1 2 3])
(función-coll?-2 (+ 3 4))
(función-coll?-3 '("hola" "buenas noches" "buenas tardes"))

(defn función-decimal?-1
  [a]
  (decimal? a))

(defn función-decimal?-2
  [a]
  (decimal? a))

(defn función-decimal?-3
  [a]
  (decimal? a))

(función-decimal?-1 (+ 1M 3))
(función-decimal?-2 1.0)
(función-decimal?-3 [1 2 3 4])

(defn función-double?-1
  [a]
  (double? a))

(defn función-double?-2
  [a]
  (double? a))

(defn función-double?-3
  [a]
  (double? a))

(función-double?-1 1.0)
(función-double?-2 (+ 2.5 3))
(función-double?-3 (first [2.0 5.0 1.0]))

(defn función-float?-1
  [a]
  (float? a))

(defn función-float?-2
  [a]
  (float? a))

(defn función-float?-3
  [a]
  (float? a))

(función-float?-1 1.0)
(función-float?-2 (* 3 2.1))
(función-float?-3 (/ 4 5))

(defn función-ident?-1
  [a]
  (ident? a))

(defn función-ident?-2
  [a]
  (ident? a))

(defn función-ident?-3
  [a]
  (ident? a))

(función-ident?-1 'abc)
(función-ident?-1 :a)
(función-ident?-3 (conj [1 2 3] :b))

(defn función-indexed?-1
  [a]
  (indexed? a))

(defn función-indexed?-2
  [a]
  (indexed? a))

(defn función-indexed?-3
  [a]
  (indexed? a))

(función-indexed?-1 [1 2 3])
(función-indexed?-2 ( ["hola" "jojo"] 1))
(función-indexed?-3 [1 2 3 {:a 1 :b 2}])

(defn función-int?-1
  [a]
  (int? a))

(defn función-int?-2
  [a]
  (int? a))

(defn función-int?-3
  [a]
  (int? a))

(función-int?-1 25.0)
(función-int?-2 (+ 4 9))
(función-int?-3 (first [1/2 1/3 1/4]))

(defn función-integer?-1
  [a]
  (integer? a))

(defn función-integer?-2
  [a]
  (integer? a))

(defn función-integer?-3
  [a]
  (integer? a))

(función-integer?-1 10)
(función-integer?-2 (+ 3 6 8))
(función-integer?-3 (count [\A \B \C]))

(defn función-keyword?-1
  [a]
  (keyword? a))

(defn función-keyword?-2
  [a]
  (keyword? a))

(defn función-keyword?-3
  [a]
  (keyword? a))


(función-keyword?-1 :a.b.c)
(función-keyword?-2 {:a 1 :b 2 :c 10})
(función-keyword?-3 '(:a 1 :a 2 3 4))

(defn función-list?-1
  [a]
  (list? a))

(defn función-list?-2
  [a]
  (list? a))

(defn función-list?-3
  [a]
  (list? a))

(función-list?-1 '(1 2 3 4 5 6))
(función-list?-2 (range 4))
(función-list?-3 (conj '("hola" "adios") '(1 2 3 4)))

(defn función-map-entry?-1
  [a]
  (map-entry? a))

(defn función-map-entry?-2
  [a]
  (map-entry? a))

(defn función-map-entry?-3
  [a]
  (map-entry? a))

(función-map-entry?-1 (first {:a 10 :b 20 :c 30}))
(función-map-entry?-2 {:a 4 :c 10})
(función-map-entry?-3 "hola")

(defn función-map?-1
  [a]
  (map? a))

(defn función-map?-2
  [a]
  (map? a))

(defn función-map?-3
  [a]
  (map? a))


(función-map?-1 {:a "hola" :b "20"})
(función-map?-2  (conj {:c 40 :d 120} {:a 10 :b 100}))
(función-map?-3 (vector "hola"))

(defn función-nat-int?-1
  [a]
  (nat-int? a))

(defn función-nat-int?-2
  [a]
  (nat-int? a))

(defn función-nat-int?-3
  [a]
  (nat-int? a))

(función-nat-int?-1 999)
(función-nat-int?-2 (* 999 -1000))
(función-nat-int?-3 (/ 50 10))

(defn función-number?-1
  [a]
  (number? a))

(defn función-number?-2
  [a]
  (number? a))

(defn función-number?-3
  [a]
  (number? a))

(función-number?-1 10)
(función-number?-2 (first [true false true false]))
(función-number?-3 (count {:a 10 :b 100 :c 200}))

(defn función-pos-int?-1
  [a]
  (pos-int? a))

(defn función-pos-int?-2
  [a]
  (pos-int? a))

(defn función-pos-int?-3
  [a]
  (pos-int? a))

(función-pos-int?-1 21345)
(función-pos-int?-2  (+ 50 -100))
(función-pos-int?-3 (first [10 true 20 false]))

(defn función-ratio?-1
  [a]
  (ratio? a))

(defn función-ratio?-2
  [a]
  (ratio? a))

(defn función-ratio?-3
  [a]
  (ratio? a))


(función-ratio?-1 5/4)
(función-ratio?-2 (- 1/2 1/4))
(función-ratio?-3 (max 1 3 4 21/4))

(defn función-rational?-1
  [a]
  (rational? a))

(defn función-rational?-2
  [a]
  (rational? a))

(defn función-rational?-3
  [a]
  (rational? a))

(función-rational?-1 10)
(función-rational?-2 (+ 2/1 3))
(función-rational?-3 (* 0.10 0.5 0.5))

(defn función-seq?-1
  [a]
  (seq? a))

(defn función-seq?-2
  [a]
  (seq? a))

(defn función-seq?-3
  [a]
  (seq? a))

(función-seq?-1 '(1 2 3))
(función-seq?-2 (range 10))
(función-seq?-3 (seq [true false true]))

(defn función-seqable?-1
  [a]
  (seqable? a))

(defn función-seqable?-2
  [a]
  (seqable? a))

(defn función-seqable?-3
  [a]
  (seqable? a))

(función-seqable?-1 [])
(función-seqable?-2 (conj {} {}))
(función-seqable?-3 (first [nil true false]))

(defn función-sequential?-1
  [a]
  (sequential? a))

(defn función-sequential?-2
  [a]
  (sequential? a))

(defn función-sequential?-3
  [a]
  (sequential? a))


(función-sequential?-1 [1 2 3 4 5 6])
(función-sequential?-2 (range 1 10))
(función-sequential?-3 (#{1 2 3 4 5} #{6 7 8 9}))

(defn función-set?-1
  [a]
  (set? a))

(defn función-set?-2
  [a]
  (set? a))

(defn función-set?-3
  [a]
  (set? a))

(función-set?-1 #{1 2 3 4})
(función-set?-2 (vector 1 2 3 5))
(función-set?-3 (conj #{1 2 3 4} #{5 6 7 8}))

(defn función-some?-1
  [a]
  (some? a))


(defn función-some?-2
  [a]
  (some? a))

(defn función-some?-3
  [a]
  (some? a))

(función-some?-1 45)
(función-some?-2 (count '( 1 2 3 4 5)))
(función-some?-3 [])

(defn función-string?-1
  [a]
  (string? a))

(defn función-string?-2
  [a]
  (string? a))

(defn función-string?-3
  [a]
  (string? a))

(función-string?-1 "hola")
(función-string?-2 (count ["Alexis" "Morales" "Martinez"]))
(función-string?-3 (first '("plf01" "plf02")))

(defn función-symbol?-1
  [a]
  (symbol? a))

(defn función-symbol?-2
  [a]
  (symbol? a))

(defn función-symbol?-3
  [a]
  (symbol? a))

(función-symbol?-1 'z)
(función-symbol?-2 ['a 'b 'c 'd])
(función-symbol?-3 (range 8))

(defn función-vector?-1
  [a]
  (vector? a))

(defn función-vector?-2
  [a]
  (vector? a))

(defn función-vector?-3
  [a]
  (vector? a))

(función-vector?-1 ["hola" "buenas noches"])
(función-vector?-2 (vector 1 2 3))
(función-vector?-2 '(1 2 3 4 5 6 7))

;Funciones de Orden Superior corregidas

(defn función-drop-1
  [a b]
  (drop a b))

(defn función-drop-2
  [a b]
  (drop a b))

(defn función-drop-3
  [a b]
  (drop a b))

(función-drop-1 2 [5 6 7 8 9 10 11])
(función-drop-2 4 #{3 4 5 1 2 7 8 9})
(función-drop-1  6 [5 6 7 8 9 10 11])

(defn función-drop-last-1
  [a]
  (drop-last a))

(defn función-drop-last-2
  [a b]
  (drop-last a b))

(defn función-drop-last-3
  [a b]
  (drop-last a b))

(función-drop-last-1 [5 4 3 2 1])
(función-drop-last-2 2 '(1 3 5 6 7))
(función-drop-last-3 3 {:a 1 :b 3 :c 5 :d 6 :e 7 :f 10})

(defn función-drop-while-1
  [a b]
  (drop-while a b))

(defn función-drop-while-2
  [a b]
  (drop-while a b))

(defn función-drop-while-3
  [a b]
  (drop-while a b))

(función-drop-while-1 neg? [-2 -4 -3 4 5 6 0 2])
(función-drop-while-2 #(>= 20 %) [10 20 30 40 50])
(función-drop-while-3 #(> 50 %) [10 20 30 40 50 80 90 100])


(defn función-every?-1
  [a]
  (every? even?  a))

(defn función-every?-2
  [a b]
  (every? a b))

(defn función-every?-3
  [a b]
  (every? a b))

(función-every?-1 '(2 4 6 8 10))
(función-every?-2 #{2 4 6} [2 4 6])
(función-every?-3 {2 "hola" 4 "adios"} [2 4])

(defn función-filterv-1
  [a b]
  (filterv a b))

(defn función-filterv-2
  [a b]
  (filterv a b))

(defn función-filterv-3
  [a b]
  (filterv a b))

(función-filterv-1 odd? [1 2 3 4 5 6 7])
(función-filterv-2 pos? [1 2 3 -4 -5 -6 7])
(función-filterv-3 zero? '(1 2 3 -4 -5 -6 0))

(defn función-group-by-1
  [a b]
  (group-by a b))

(defn función-group-by-2
  [a b]
  (group-by a b))

(defn función-group-by-3
  [a b]
  (group-by a b))

(función-group-by-1 count ["hola" "buenas" "tardes" "noches"])
(función-group-by-2 :nombre [{:nombre "alexis" :apellido "morales"}
                     {:nombre "kevin" :apellido "suarez"}
                     {:nombre "nadia" :apellido "mmorelia"}])
(función-group-by-3 even? '(1 2 4 5 7 8 9 12 14 15 17))

(defn función-iterate-1
  [a b]
  (iterate a b))

(defn función-iterate-2
  [a b]
  (take a b))

(defn función-iterate-3
  [a b]
  (take a b))

(función-iterate-1 inc 2)
(función-iterate-2 2 (iterate inc 10))
(función-iterate-3 5 (iterate (partial + 3) 0))

(defn función-keep-1
  [a b]
  (keep a b))

(defn función-keep-2
  [a b]
  (keep a b))

(defn función-keep-3
  [a b]
  (keep a b))

(función-keep-1 odd? (range 1 5))
(función-keep-2 int? [2 4.4 3 5.5])
(función-keep-3 string? '(\a "hola" \b "plf"))


(defn función-keep-indexed-1
  [a b]
  (keep-indexed a b))

(defn función-keep-indexed-2
  [a b]
  (keep-indexed a b))

(defn función-keep-indexed-3
  [a b]
  (keep-indexed a b))

(función-keep-indexed-1  list [:a :b :c :d])
(función-keep-indexed-2 vector [-9 0 29 -7 45 3 -8])
(función-keep-indexed-3 hash-map [2 4 6 8 10 11 13])

(defn función-map-indexed-1
  [a b]
  (map-indexed a b))

(defn función-map-indexed-2
  [a b]
  (map-indexed a b))

(defn función-map-indexed-3
  [a b]
  (map-indexed a b))

(función-map-indexed-1  vector "hola")
(función-map-indexed-2  hash-map '("hola" 1 2 "buenas" 3))
(función-map-indexed-3 list {:a 1 :b 2 :c 3})

(defn función-mapcat-1
  [a b]
  (mapcat a b))

(defn función-mapcat-2
  [a b]
  (mapcat a b))
  
(defn función-mapcat-3
  [a b]
  (mapcat a b))

(función-mapcat-1 reverse [[5 4 3 2] [7 6] [10 9 8]])
(función-mapcat-2 list [[3 4 5] [6 6] [10 8]])
(función-mapcat-3 repeat '(2 3 "hello " 4 5 "plf0"))

(defn función-mapv-1
  [a b]
  (mapv a b))

(defn función-mapv-2
  [a b c]
  (mapv a b c))

(defn función-mapv-3
  [a b c]
  (mapv a b c))

(función-mapv-1 dec [3 5 6 7 8])
(función-mapv-2 * [1 2 3] [4 5 6])
(función-mapv-3 + '(2 4 6) (iterate inc 2))

(defn función-merge-with-1
  [a b c]
  (merge-with a b c))

(defn función-merge-with-2
  [a b c d]
  (merge-with a b c d))

(defn función-merge-with-3
  [a b c]
  (merge-with a b c))

(función-merge-with-1 + {:a 3 :b 6 :c 9}
                      {:a 1 :b 2 :c 3})
(función-merge-with-2 * {:a 3} {:a 1} {:a 4})
(función-merge-with-3 into {:a #{2 4 6},   :b #{7 8 9}}
                      {:a #{1 5 7 8}, :c #{1 2 3}})


(defn función-not-any?-1
  [a b]
  (not-any? a b))

(defn función-not-any?-2
  [a b]
  (not-any? a b))

(defn función-not-any?-3
  [a b]
  (not-any? a b))

(función-not-any?-1 even? [1 2 3 4 5 8 9])
(función-not-any?-2 odd? '( 2 4 5 8 ))
(función-not-any?-3 nil? [true false true false nil])

(defn función-not-every?-1
  [a b]
  (not-every? a b))

(defn función-not-every?-2
  [a b]
  (not-every? a b))

(defn función-not-every?-3
  [a b]
  (not-every? a b))

(función-not-every?-1 even? [1 2 3])
(función-not-every?-2 char? '(\A))
(función-not-every?-3 string? '("hola" "buenas" true))

(defn función-partition-by-1
  [a b]
  (partition-by a b))

(defn función-partition-by-2
  [a b]
  (partition-by a b))

(defn función-partition-by-3
  [a b]
  (partition-by a b))

(función-partition-by-1 odd? [3 3 3 2 2 5 6])
(función-partition-by-2 count '("alexis" "morales" "martinez"))
(función-partition-by-3 #(= 3 %) '(-1 -2 1 2 3 4 5))

(defn función-reduce-kv-1
  [a b c]
  (reduce-kv a b c))

(defn función-reduce-kv-2
  [a b c]
  (reduce-kv a b c))

(defn función-reduce-kv-3
  [a b c]
  (reduce-kv a b c))

(función-reduce-kv-1 #(assoc %1 %3 %2) {} {:a 2 :b 4 :c 6})
(función-reduce-kv-2 #(assoc %1 %3 %1) {} {:d 3 :e 5 :f 9})
(función-reduce-kv-3 #(assoc %1 %1 %3) {} {:d 51 :e 04 :f 13})

(defn función-remove-1
  [a b]
  (remove a b))

(defn función-remove-2
  [a b]
  (remove a b))

(defn función-remove-3
  [a b]
  (remove a b))

(función-remove-1 pos? [4 -3 -2 5 6 -1 0])
(función-remove-2 integer? '(1 2 5.5 3 8.8 4 10.2 11))
(función-remove-3 #{1 2 3} #{3 5 6 1 8 9})

(defn función-reverse-1
  [a]
  (reverse a))

(defn función-reverse-2
  [a]
  (reverse a))

(defn función-reverse-3
  [a]
  (reverse a))

(función-reverse-1 [4 5 6 7])
(función-reverse-2 "hola")
(función-reverse-3 (filterv pos? [4 5 -6 -7 8 9]))

(defn función-some-1
  [a b]
  (some a b))

(defn función-some-2
  [a b]
  (some a b))

(defn función-some-3
  [a b]
  (some a b))

(función-some-1 true? [true false true])
(función-some-2  #(= 8 %) [1 2 4 6 8 10])
(función-some-3 char? '("hola" 1 "dos"))

(defn función-sort-by-1
  [a b]
  (sort-by a b))

(defn función-sort-by-2
  [a b]
  (sort-by a b))

(defn función-sort-by-3
  [a b]
  (sort-by a b))

(función-sort-by-1 count [[3 4] [2 2] [6 6]])
(función-sort-by-2 count {:a 7 :b 3 :c 5})
(función-sort-by-3 count ["plf01" "plf023" "holi" "a" "holi1"])

(defn función-split-with-1
  [a b]
  (split-with a b))

(defn función-split-with-2
  [a b]
  (split-with a b))

(defn función-split-with-3
  [a b]
  (split-with a b))

(función-split-with-1 odd? [1 3 5 6 7 9 10 11 13])
(función-split-with-2  (partial > 5) [2 4 6 7 8 10 12 14 16 18])
(función-split-with-3  (partial > 3) '(-3 -5 -6 -10 11 13))

(defn función-take-1
  [a b]
  (take a b))

(defn función-take-2
  [a b]
  (take a b))

(defn función-take-3
  [a b]
  (take a b))

(función-take-1 2 [3 5 6 7 8 9])
(función-take-2 2 (conj [1 2 6] [7 8 9]))
(función-take-3 3 (drop 9 (range 1 20)))

(defn función-take-last-1
  [a b]
  (take-last a b))

(defn función-take-last-2
  [a b]
  (take-last a b))

(defn función-take-last-3
  [a b]
  (take-last a b))

(función-take-last-1 2 [2 4 5 7 9])
(función-take-last-2 3 '(\A \B \C \D \F))
(función-take-last-3 2 #{1 3 4 5 6 7 8})

(defn función-take-nth-1
  [a b]
  (take-nth a b))

(defn función-take-nth-2
  [a b]
  (take-nth a b))

(defn función-take-nth-3
  [a b]
  (take-nth a b))

(función-take-nth-1 2 (range 5))
(función-take-nth-2 1 (conj [1 2 3] [4 5 6]))
(función-take-nth-3 3 '(a b c d e f g h))

(defn función-take-while-1
  [a b]
  (take-while a b))

(defn función-take-while-2
  [a b]
  (take-while a b))

(defn función-take-while-3
  [a b]
  (take-while a b))

(función-take-while-1 #{[10 11] [8 9]} #{[10 11]})
(función-take-while-2 pos? [5 6 -1 -2 6 7])
(función-take-while-3 string? ["plf" \A 1 2])

(defn función-update-1
  [a b c]
  (update a b c))

(defn función-update-2
  [a b c]
  (update a b c))

(defn función-update-3
  [a b c d]
  (update a b c d))

(función-update-1 [2 4 6] 1 inc)
(función-update-2 [1 2 3 4] 1 dec)
(función-update-3 [1 3 5 8] 3 * 2)


(defn función-update-in-1
  [a b c]
  (update-in a b c))

(defn función-update-in-2
  [a b c]
  (update-in a b c))

(defn función-update-in-3
  [a b c d e]
  (update-in a b c d e))

(función-update-in-1 [1 2 [3 4 5]] [2 0] inc)
(función-update-in-2 [3 4 [5 6 7]] [2 0] dec)
(función-update-in-3 {:a 5} [:a] + 5 3)

;Corregifo y probado